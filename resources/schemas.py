from django.http import HttpResponse

import json


class JSONResponse():

    def __init__(self, resource):
        with open('resources/schemas/{0}.json'.format(resource)) as f:
            data = json.loads(f.read())
        self.data = data

    @property
    def response(self):
        return HttpResponse(
            json.dumps(self.data),
            content_type="application/json"
        )


def characters(request):
    return JSONResponse('characters').response

def creatures(request):
    return JSONResponse('creatures').response

def species(request):
    return JSONResponse('species').response

def races(request):
    return JSONResponse('races').response

def susceptibilities(request):
    return JSONResponse('susceptibilities').response