from django.shortcuts import render

from rest_framework import viewsets

from .models import (
    Character,
    Creature,
    Race,
    Species,
    Susceptibility,
)

from .serializers import (
    CharacterSerializer,
    CreatureSerializer,
    RaceSerializer,
    SpeciesSerializer,
    SusceptibilitiesSerializer
)

class CharacterViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Character.objects.all()
    serializer_class = CharacterSerializer

    def retrieve(self, request, *args, **kwargs):
        return super(CharacterViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super(CharacterViewSet, self).list(request, *args, **kwargs)

class CreatureViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Creature.objects.all()
    serializer_class = CreatureSerializer

    def retrieve(self, request, *args, **kwargs):
        return super(CreatureViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super(CreatureViewSet, self).list(request, *args, **kwargs)

class RaceViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Race.objects.all()
    serializer_class = RaceSerializer
    lookup_field = 'id'

    def retrieve(self, request, *args, **kwargs):
        return super(RaceViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super(RaceViewSet, self).list(request, *args, **kwargs)

class SpeciesViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer
    lookup_field = 'id'

    def retrieve(self, request, *args, **kwargs):
        return super(SpeciesViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super(SpeciesViewSet, self).list(request, *args, **kwargs)

class SusceptibilitiesViewSet(viewsets.ReadOnlyModelViewSet):

    queryset = Susceptibility.objects.all()
    serializer_class = SusceptibilitiesSerializer
    lookup_field = 'id'
    #many=True

    def retrieve(self, request, *args, **kwargs):
        return super(SusceptibilitiesViewSet, self).retrieve(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        return super(SusceptibilitiesViewSet, self).list(request, *args, **kwargs)