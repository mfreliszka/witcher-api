from rest_framework import serializers

from .models import (
    Creature,
    Species,
    Race,
    Character,
    Susceptibility
)

class SusceptibilitiesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Susceptibility
        fields = (
            "id",
            "name",
            "edited",
            "representatives",
            #"url"
        )

class RaceSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Race
        fields = (
            "id",
            "name",
            "edited",
            "representatives"
            #"url"
        )

class SpeciesSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Species
        fields = (
            "id",
            "name",
            "edited",
            "representatives",
            #"url"
        )

class CharacterSerializer(serializers.HyperlinkedModelSerializer):
    
    race = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name="race-detail",
        lookup_field = 'id'
    )

    class Meta:
        model = Character
        fields = (
            "id",
            "name",
            "race",
            "gender",
            "edited",
            "url"
        )

class CreatureSerializer(serializers.HyperlinkedModelSerializer):

    species = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name="species-detail",
        lookup_field = 'id'
    )
    
    susceptibilities = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name="susceptibility-detail",
        lookup_field = 'id',
        many = True
    )

    class Meta:
        model = Creature
        fields = (
            "id",
            "name",
            "species",
            "susceptibilities",
            "edited",
            "url"
        )