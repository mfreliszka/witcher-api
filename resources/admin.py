from django.contrib import admin

from .models import (
    Character,
    Creature,
    Race,
    Species,
    Susceptibility
)

classes = [Character, Creature, Race, Species, Susceptibility]

for c in classes:
    admin.site.register(c)