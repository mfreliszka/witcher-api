from django.db import models

class DateTimeModel(models.Model):
    """ A base model with created and edited datetime fields """

    class Meta:
        abstract = True

    created = models.DateTimeField(auto_now_add=True)
    edited = models.DateTimeField(auto_now=True)

class Susceptibility(DateTimeModel):

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "susceptibilities"  

    name = models.CharField(max_length=100)


class Race(DateTimeModel):
    
    def __str__(self):
        return self.name
        
    name = models.CharField(max_length=100)

class Species(DateTimeModel):
    
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "species"    
    name = models.CharField(max_length=100)

class Character(DateTimeModel):

    def __str__(self):
        return self.name

    name = models.CharField(max_length=100)
    race = models.ForeignKey(Race, related_name="representatives", on_delete=models.DO_NOTHING)
    gender = models.CharField(max_length=40, blank=True)

class Creature(DateTimeModel):

    def __str__(self):
        return self.name
        
    name = models.CharField(max_length=100)
    species = models.ForeignKey(Species, related_name="representatives", on_delete=models.DO_NOTHING)
    susceptibilities = models.ManyToManyField(Susceptibility, related_name="representatives")#, on_delete=models.DO_NOTHING)


