# Witcher API #
#### Project just started, under maintenance... ###

This repository contains api project for the Witcher 3: The Wild Hunt game world, written in Python and Django Rest Framework.

## Available online ##
### Getting json data from api ###
`GET https://mfreliszka.pythonanywhere.com/api/`

## Available offline ##
### Installing requirements ###
Use commands below in main repository folder.

`pip install -r requirements.txt`

`python manage.py runserver`

### Getting json data from api ###

`GET localhost:8000/api/`