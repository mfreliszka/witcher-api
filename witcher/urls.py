"""witcher URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from resources.schemas import creatures, characters, races, species, susceptibilities
from .views import index, documentation, about
from django.views.generic import TemplateView
admin.autodiscover()

from rest_framework import routers

from resources import views

router = routers.DefaultRouter()

router.register(r"characters", views.CharacterViewSet)
router.register(r"creatures", views.CreatureViewSet)
router.register(r"races", views.RaceViewSet)
router.register(r"species", views.SpeciesViewSet)
router.register(r"susceptibilities", views.SusceptibilitiesViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r"^$", TemplateView.as_view(template_name='index.html')),
    url(r"^documentation$", TemplateView.as_view(template_name='documentation.html')),
    url(r"^about$", TemplateView.as_view(template_name='about.html')),
    url(r"^api/creatures/schema$", creatures),
    url(r"^api/characters/schema$", characters),
    url(r"^api/races/schema$", races),
    url(r"^api/species/schema$", species),
    url(r"^api/susceptibilities/schema$", susceptibilities),
    url(r"^api/", include(router.urls))
]
